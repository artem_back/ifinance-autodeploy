#!/bin/bash
cd /var/www/$1/;
cp /etc/.env.$2 .env;
php yii drop;
php yii migrate --interactive=0;
#php yii cms/install;
php yii model;
php yii insert --interactive=0;
mkdir backend/web/assets;
mkdir frontend/web/assets;
mkdir frontend/web/uploads;
mkdir frontend/runtime/wfp;
touch frontend/runtime/wfp/response.log;
mkdir console/runtime/cron;

cd frontend/web/uploads/;
cp /home/ubuntu/dumps/media_uploads.zip ./;
unzip -o media_uploads.zip;
rm media_uploads.zip;

mysql -udevelop_ifinance -p1g1?X@FUhX0S develop_ifinance < /home/ubuntu/dumps/cms_dump.sql;
mysql -ustage_ifinance -pgn5GETkrJ33ZWPrQ stage_ifinance < /home/ubuntu/dumps/cms_dump.sql;
mysql -umvpuser -pmvppassword mvp_ifinance < /home/ubuntu/dumps/cms_dump.sql;
