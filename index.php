<?php
$request = json_decode(file_get_contents('php://input'), true);

$log = fopen('/var/www/autodeploy/build.log', 'a');
fwrite($log, date("Y-m-d H:i:s"));

if ($request['key'] != getenv('IFINANCE_DEPLOYMENT_SECRET_KEY')) {
	fwrite($log, "\n".'wrong deployment key');
}

$branchesConfig = [
	'deployment' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_deployment.zip',
		'doc_root' => 'deployment',
	],
	'develop' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_develop.zip',
		'doc_root' => 'dev.ifinance.biz.ua',
	],
	'stage' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_stage.zip',
		'doc_root' => 'ifinance.biz.ua',
	],
	'stage-2-1' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_stage-2-1.zip',
		'doc_root' => 'ifinance.biz.ua',
	],
	'stage-2-2' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_stage-2-2.zip',
		'doc_root' => 'ifinance.biz.ua',
	],
	'stage-2-3' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_stage-2-3.zip',
		'doc_root' => 'ifinance.biz.ua',
	],
	'stage-2-4' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_stage-2-4.zip',
		'doc_root' => 'ifinance.biz.ua',
	],
	'mvp' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_mvp.zip',
		'doc_root' => 'mvp.ifinance.ua',
	],
	'release-2-1' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_release-2-1.zip',
		'doc_root' => 'mvp.ifinance.ua',
	],
	'release-2-2' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_release-2-2.zip',
		'doc_root' => 'mvp.ifinance.ua',
	],
	'release-2-3' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_release-2-3.zip',
		'doc_root' => 'mvp.ifinance.ua',
	],
	'2-0' => [
		'bucket' => 'prooven.ifinance.biz',
		'key' => 'ifinance_2-0.zip',
		'doc_root' => 'ifinance.biz.ua'
	],
];

//if (!isset($branchesConfig[$request['branch']])) {
//	fwrite($log, "\n".'request is not containing branch name');
//}
require_once 'vendor/autoload.php';

define('DL', '/');
define('BUILD_DIR', 'builds' . DL);
define('SERVER_ROOT', DL . 'var' . DL . 'www' . DL);
define('BUILD_EXT', '.build');

echo SERVER_ROOT;
echo BUILD_DIR;

$config = $branchesConfig[$request['branch']];

$s3 = new Aws\S3\S3Client([
	'version' => 'latest',
	'region'  => 'eu-central-1'
]);
echo "Created S3 client...\n";
$result = $s3->getObject([
	'Bucket' => $config['bucket'],
	'Key'    => $config['key'],
	'SaveAs' => BUILD_DIR . $config['key']
]);
echo "Saved S3 data $result";
if ($result) {
	fwrite($log, "\n".'downloaded archive');
}

$zip = new ZipArchive;

if (!$zip->open(BUILD_DIR . $config['key'])) {
	fwrite($log, "\n".'failed to open archive');
}

if (!is_dir(SERVER_ROOT . $config['doc_root'] . BUILD_EXT)) {
	fwrite($log, "\n".'no previous build dir. creating ' . SERVER_ROOT . $config['doc_root'] . BUILD_EXT);
	mkdir(SERVER_ROOT . $config['doc_root'] . BUILD_EXT);
}

(mkdir(SERVER_ROOT . $config['doc_root'] . BUILD_EXT))
	? fwrite($log, "\n".'created  ' . SERVER_ROOT . $config['doc_root'] . BUILD_EXT)
	: fwrite($log, "\n".'failed to create  ' . SERVER_ROOT . $config['doc_root'] . BUILD_EXT);
($zip->extractTo(SERVER_ROOT . $config['doc_root'] . BUILD_EXT))
	? fwrite($log, "\n".'extracted to  ' . SERVER_ROOT . $config['doc_root'] . BUILD_EXT)
	: fwrite($log, "\n".'can\'t extract archive to  ' . SERVER_ROOT . $config['doc_root'] . BUILD_EXT);
$zip->close();
if (is_dir(SERVER_ROOT . $config['doc_root'] . BUILD_EXT)) {
	fwrite($log, "\n".'the dir is exist  ' . SERVER_ROOT . $config['doc_root'] . BUILD_EXT);
	exec('rm -rf ' . SERVER_ROOT . $config['doc_root']);

	(rename(
		SERVER_ROOT . $config['doc_root'] . BUILD_EXT,
		SERVER_ROOT . $config['doc_root']
	))
		? fwrite($log, "\n".'rename was succesfull')
		: fwrite($log, "\n".'rename was failed');
}

rmdir(BUILD_DIR . $config['doc_root'] . '*');

fclose($log);

exec('chmod +x postdeploy_actions.sh');
exec("./postdeploy_actions.sh {$config['doc_root']} {$request['branch']}");

die('done');
